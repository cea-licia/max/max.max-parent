# max.parent

Parent pom for all MAX modules.

It defines:

- The minimum maven version
- Source and target Java versions
- Files encoding
- Common dependencies
- Plugins configuration